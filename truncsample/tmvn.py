#!/usr/bin/env python

import numpy as np
import scipy.linalg as linalg
import cvxpy as cvx
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.stats import uniform
from scipy.stats import truncnorm

class truncmvn:
    """
    TRUNCATED MULTIVARIATE NORMAL DISTRIBUTION
    
    The :py:func:`.truncmvn` class generates an instance of a linearly truncated multivariate normal distribution (TMVN). The number of variables (dimensions) is given as *D*. We assume the TMVN is specified by the following parameters:
    
    #. A *D*-dimensional mean vector : *mu*
    
    #. A *DxD*-dimensional covariance or precision matrix : *M*
    
    #. A *CxD*-dimensional constraint matrix : *F*
    
    #. A *C*-dimensional constraint vector : *g*
    
    #. A boolean indicator which indicates if the provided matrix *M* is a covariance matrix: *cov*
    
    
    The density function evaluated at a point *x* in *D*-dimensional space then takes the following form:
    
    * if *F . x <= g* : *pdf(x) = C . (x-mu)´ . Sigma^-1 . (x-mu)*
    
    * otherwise       : *pdf(x) = 0*
    
    with *Sigma* the covariance matrix (*Sigma=M* if cov=True, *Sigma=1/M* otherwise) and *C* a (usually unknown) normalization constant.
    
    Parameters
    ----------
    mu : mean vector
    M : covariance matrix / precision matrix
    F : Constraint matrix (left-hand side)
    g : Constraint vector (right-hand side)
    cov : Boolean indicator, True of *M* is a covariance matrix, False otherwise
    verbose : Boolean indicator, True of you want verbose command line output
    
    Returns
    -------
    tmvn: Object describing the TMVN
    
    """
    
    def __init__(self, mu, M, F, g, cov, verbose=False):
        """Initialization. See class description for documentation"""
        
        self.verbose  =  verbose
        
        if self.verbose:
            print ("Instantiation")
            
        self.mu   =   mu
        self.M    =   M
        self.F    =   F
        self.g    =   g
        self.cov  =   cov
        
    def getmode(self):
        """GETMODE
        
        The purpose of :py:func:`.truncmvn.getmode` is to obtain the mode of the TMVN (expressed in original scale).
        
        Parameters
        ----------
        
        Returns
        -------
        ymode : *D*-dimensional vector specifying the mode
        
        """
        
        if self.verbose:
            print ("Compute mode")
        
        x            =   cvx.Variable (2)
        Fw           =   self.Fw
        gw           =   self.gw
        constraints  =   [  Fw*x  +  gw  >=  0  ]
        prob         =   cvx.Problem (cvx.Minimize (cvx.pnorm (x, p=2)) , constraints)
        prob.solve ()
        xmode        =   x.value
        ymode        =   self.unwhiten (xmode)
        self.mode    =   ymode
        
        return ymode
    
    def rejection(self,L,verbose=False,visual=False):
        """REJECTION SAMPLER
        
        The purpose of :py:func:`.truncmvn.rejection` is to obtain samples from the TMVN by means of rejection sampling.
        
        Parameters
        ----------
        L : Number of samples
        verbose : Boolean indicator for command line output (True/False)
        visual : Boolean indicator for graphical output (True/False)
    
        Returns
        -------
        Y : *LxD*-dimensional numpy array with collected samples (one row = one sample)
        
        """
        if self.verbose | verbose:
            print ("... rejection sampler")
        Lsuccess     =   0    # Totel number of accepted samples
        Ltrials      =   0    # Totel number of tried samples
        rate         =   1/2  # Initial guess of acceptance rate
        X            =   np.ones ((self.dim,L))*np.nan
        while True:
            
            # Define number of new samples:
            Ldeficit     =   L - Lsuccess                 # Total number of missing samples
            Ltest        =   np.ceil (Ldeficit/rate)      # Total number of samples that will be tested
            Ltest        =   int (np.min ((int(1e7),Ltest)))
            
            # Create, test, select new samples:
            Xtest        =   norm.rvs (size=(self.dim,Ltest)) # Generate samples
            tf           =   np.all (self.Fw.dot (Xtest)+self.gw[:,None]>=0, axis=0) # Test whether they are accepted
            Xacc         =   Xtest[:,np.where (tf)[0]] # Select the accepted samples
            if visual & (self.dim==2):
                Xrej     =   Xtest[:,np.where (tf==False)[0]] # Select the rejected samples
                Yrej     =   self.unwhiten(Xrej)
                Yacc     =   self.unwhiten(Xacc)
                plt.plot(Yrej[0,:],Yrej[1,:],'r.')
                plt.plot(Yacc[0,:],Yacc[1,:],'b.')
                
            # Add selected samples to stack:
            Laccept      =   np.sum(tf) # Count the number of accepted samples
            Ladd         =   np.min((Laccept,Ldeficit)) # Count the number of samples that are added
            col          =   np.arange(0,Ladd) # Indices of samples that are added
            colT         =   Lsuccess + col
            X[:,colT]    =   Xacc[:,col] # Add the accepted samples
            
            # Succes rate estimation:
            Ltrials      =   Ltrials + Ltest # Update the total number of trials
            Lsuccess     =   Lsuccess + Laccept # Update the total number of accepted samples
            rate         =   np.max ((Lsuccess,1)) / Ltrials # Update the acceptance rate estimate
            
            if verbose:
                print ("... rejection sampler - trials: " + str (Ltrials) + " - successes: " + str (Lsuccess))
                pass
            
            if Lsuccess>=L:
                break
                
            if Ltrials>=1e8:
                print ("... rejection sampler - total number of trials has reached " + str (Ltrials) + " without reaching target of " + str (L) + " successes. I quit!")
                col      =   np.arange (0,Lsuccess) # Indices of samples that are added
                X        =   X[:,col]
                break
        
        Y            =   self.unwhiten (X)
        
        if self.verbose | verbose:
            fac          =   1000
            rate         =   Lsuccess / Ltrials # Acceptance rate estimate
            print ("... rejection sampler - observed success rate [%]: " + str (np.ceil (rate*100*fac)/fac))
        return Y
    
    def sample(self,L,y0=None,verbose=False,visual=False,sampler=None):
        
        """MARKOV CHAIN MONTE CARLO SAMPLER
        
        The purpose of :py:func:`.truncmvn.sample` is to obtain a number of samples from the TMVN by means of iterative application of a Markov chain Monte Carlo sampler of your choice.
        
        Parameters
        ----------
        L      : Number of samples
        y0 : Initial sample [optional, default is the mode of the TMVN]
        verbose       : Boolean indicator for command line output (True/False) [optional, default=False]
        visual       : Boolean indicator for graphical output (True/False) [optional, default=False]
        sampler       : String specifying the chosen sampler [optional, default='rejection'], available options: 'gibbs', 'hitandrun', 'hmc', 'rejection'
    
        Returns
        -------
        * Y    : *LxD*-dimensional numpy array with collected samples (one row = one sample)
    
        * bounces    : Number of bounces (*L*-dimensional numpy array); only meaningful when sampler='hmc'; defaults to null vector for other samplers
        
        """
        if verbose:
            print ("trunc.tmvn.sampling ...")
        
        L = int (L)
        bounces = np.zeros(L)
        
        if sampler==None:
            sampler  =   "rejection"
            
        if sampler=="rejection":
            Y        =   self.rejection (L,verbose,visual)
        else:
            X        =   np.ones ((self.dim,L))*np.nan
        
            if y0 is None:
                y0   =   self.getmode
        
            x_init   =   self.whiten (y0)
        
            if verbose:
                print ("Rotate y: " + str (y0))
                print ("Rotate x: " + str (x_init))
                print (self.cholM)
        
            # Verify that initial_X is feasible
            c        =   self.Fw.dot (x_init) + self.gw
            if np.any (c<0):
                print ('  SAMPLER - ERROR: infeasible initial condition')
            else:
                if verbose:
                    print ('  SAMPLER - OK: feasible initial condition')
                pass
            
            x        =   x_init
            
            i        =   0
            X[:,i]   =   x
            
            if visual & (self.dim==2):
                plt.plot(y0[0],y0[1],'ro')
                
                
            i        =   1
            while i<L:
                
                if verbose:
                    print ("SAMPLE: " + str(i))
                    
                if sampler=='gibbs':
                    x    =   self.sample_gibbs(x,verbose,visual)
                elif sampler=='hitandrun':
                    x    =   self.sample_hitandrun(x,verbose,visual)
                elif sampler=='hmc':
                    x,bouncenum    =   self.sample_hmc(x,verbose,visual,Tau=np.pi/2,AbsTol=10000*np.finfo(float).eps)
                    bounces[i] = bouncenum
                else:
                    print ('ERROR: Unknown sampler specification')
                
                X[:,i]   =   x
                i       +=   1
        
            Y       =   self.unwhiten(X)
            
        if verbose:
            print ("trunc.tmvn.sampling finished")
            
        return Y, bounces
    
    def sample_gibbs(self,x0,verbose=False,visual=False):
        
        """GIBBS SAMPLER
        
        The purpose of :py:func:`.truncmvn.sample_gibbs` is to obtain a single sample from the TMVN by means of a single iteration of the Gibbs sampler, starting from a provided sample *x0*.
        
        Parameters
        ----------
        x0 : Initial sample (*D*-dimensional numpy array)
        verbose       : Boolean indicator for command line output (True/False)
        visual       : Boolean indicator for graphical output (True/False)
    
        Returns
        -------
        x1    : New sample (*D*-dimensional numpy array)
        
        """
        
        x0           =   np.asarray (x0, dtype=np.float)
        
        xnew         =   x0[:]
        D            =   self.dim
        Fw           =   self.Fw
        gw           =   self.gw
        
        debug        =   False 
                
                
        for j in np.arange(D):
            
            if verbose:
                print ("Gibbs - dimension " + str(j) + " of " + str(D))
                
            if debug:
                fig           =   plt.figure (figsize=(4,4),dpi=200)
                ax            =   fig.gca ()
                plt.plot (0,0,'mo')
                plt.plot (x0[0],x0[1],'bo')

            notj = np.delete (np.linspace (0,D,D,endpoint=False,dtype = int),j)
            
            # II. Compute feasible domain
            # 1. compute points on boundaries
            ncon          =   np.shape (Fw)[0]
            xbnd          =   np.inf*np.ones (ncon)
            for icon in range(ncon):
                if Fw[icon,j] !=0:
                    xbnd[icon]    =   -(Fw[icon,notj].dot (xnew[notj,None])+gw[icon]) / Fw[icon,j]
            if debug:
                print ("Bounds:" + str (xbnd))
            xbnd          =   np.sort (xbnd[~np.isinf (xbnd)])
            nhit          =   len (xbnd)
            
            if debug:
                print ("Bounds: " + str (xbnd))
                if j==0:
                    plt.plot (xbnd,x0[notj]*np.ones (nhit),'co')
                elif j==1:
                    plt.plot (x0[notj]*np.ones (nhit),xbnd,'co')
            
            xcheck        =   np.copy(xnew)
            foundseg      =   False
            seg           =   0 
            while (seg<(nhit+1)) & (not foundseg):
                if seg==0:
                    xcheck[j]     = xbnd[seg]-1.0
                elif seg==nhit:
                    xcheck[j]     = xbnd[seg-1]+1.0
                else:
                    xcheck[j]     = (xbnd[seg-1]+xbnd[seg])/2
                if debug:
                    print ("Segment: " + str(seg) + " of " + str(nhit))
                    plt.plot (xcheck[0],xcheck[1],'b+')
            
                if np.all (np.dot (Fw,xcheck)+gw>=0):
                    foundseg      =   True    
                else:
                    seg           =   seg+1
                
            if foundseg:
                if seg==0:
                    interval      =   np.array ([-np.inf,xbnd[seg]-1])
                elif seg==nhit:
                    interval      =   np.array ([xbnd[seg-1],np.inf])
                else:
                    interval      =   np.array ([xbnd[seg-1],xbnd[seg]])
            else:
                print ("Gibbs - NO FEASIBLE INTERVAL")
                interval      = np.zeros(0)
                plt.plot (0,0,'mo')
                plt.plot (xnew[0],xnew[1],'ro')
                print (xbnd)
                print (xnew)
                for ihit in range (nhit):
                    xcheck        =   xold
                    xcheck[j]     =   xbnd[ihit]
                    plt.plot (xcheck[0],xcheck[1],'k+')
                for ihit in range (nhit+1):
                    xcheck        =   xold 
                    if ihit==0:
                        xcheck[j]     =   xbnd[ihit]-1.0
                    elif ihit==nhit:
                        xcheck[j]     =   xbnd[ihit-1]+1.0
                    else:
                        xcheck[j]     =   (xbnd[ihit-1]+xbnd[ihit]) / 2
                    plt.plot (xcheck[0],xcheck[1],'kx')
            
            if debug:
                print (interval)
            
            # III. Sample
            U             =   uniform.rvs(size=1)
            xold          =   np.copy(xnew)
            
            xnew[j]       =   truncnorm.ppf (U, interval[0], interval[1], loc=0, scale=1)
            
            if visual & (D==2):
            
                yold     =   self.unwhiten(xold)
                ynew     =   self.unwhiten(xnew)
                plt.plot((yold[0],ynew[0]),(yold[1],ynew[1]),'.-',Color=np.ones(3)*.73)
                
            c             =   self.Fw.dot (xnew) + self.gw
            if np.any (c<0):
                print ('  GIBBS - ERROR: infeasible proposal')
                fig           =   plt.figure (figsize=(8,8),dpi=100);
                plt.plot (xold[0],xold[1],'ko')
                print (xbnd)
                print (xold)
                print (xnew)
                for ihit in range (nhit):
                    xcheck        =   xold
                    xcheck[j]     =   xbnd[ihit]
                    plt.plot (xcheck[0],xcheck[1],'k+')
                for ihit in range (nhit+1):
                    xcheck        =   xold 
                    if ihit==0:
                        xcheck[j]     =   xbnd[ihit]-1.0
                    elif ihit==nhit:
                        xcheck[j]     =   xbnd[ihit-1]+1.0
                    else:
                        xcheck[j]     =   (xbnd[ihit-1]+xbnd[ihit])/2
                    plt.plot(xcheck[0],xcheck[1],'kx')
                    print (np.dot (Fw,xcheck) + gw)
                error()
            else:
                if debug:
                    plt.plot (xnew[0],xnew[1],'y+')
                if verbose:
                    #print('  GIBBS - OK: feasible proposal')
                    pass
                
        if visual & (D==2):
            plt.plot(ynew[0],ynew[1],'ko')
            
        return xnew
    
    def sample_hitandrun(self,x0,verbose=False,visual=False):
        
        """HIT-AND-RUN SAMPLER
        
        The purpose of :py:func:`.truncmvn.sample_hitandrun` is to obtain a single sample from the TMVN by means of a single iteration of the Hit-and-Run sampler, starting from a provided sample *x0*.
        
        Parameters
        ----------
        x0 : Initial sample (*D*-dimensional numpy array)
        verbose       : Boolean indicator for command line output (True/False)
        visual       : Boolean indicator for graphical output (True/False)
    
        Returns
        -------
        x1    : New sample (*D*-dimensional numpy array)
        
        """
        
        
        debug        =   False
        
        x0           =   np.asarray (x0, dtype=np.float)
        
        x_old        =   x0
        D            =   self.dim
        Fw           =   self.Fw
        gw           =   self.gw
        
        ncon         =   np.shape(Fw)[0]
        
        if debug: 
            fig          = plt.figure(figsize=(4,4),dpi=100)
            plt.plot (x_old[0],x_old[1],'ko')
            plt.plot (0,0,'k+')
        
        #RANDOM DIRECTION + SAMPLE
        v            =   norm.rvs (size=(D,1))
        v            =   v/np.linalg.norm(v)
        
        cdf          =   uniform.rvs (size=1)
        
        # DESCRIBE MARGINAL ALONG SELECTED DIMENSION
        t_mean       =   np.dot(np.transpose(v),-x_old)  
        
        if debug: 
            x_mean       =   np.outer (v,t_mean)+x_old[:,None]
            plt.plot (x_mean[0],x_mean[1],'ro')
        
        # Constrained model
        tbnd         =   np.zeros (ncon)
        for icon in range (ncon):
            a            =   gw[icon] + np.dot (Fw[icon,],x_old)
            b            =   np.dot (Fw[icon,],v)
            tbnd[icon]   =   -a/b

        if visual & (D==2):
            x_bnd        =   np.outer (v,tbnd) + x_old[:,None]
            x_mean       =   np.outer (v,t_mean)+x_old[:,None]
            y_bnd        =   self.unwhiten(x_bnd)
            y_mean       =   self.unwhiten(x_mean[:,0])
            plt.plot (y_bnd[0,:],y_bnd[1,:],'-',Color=np.ones(3)*.73)
            plt.plot (y_mean[0],y_mean[1],'.',Color=np.ones(3)*.73)
            
        tbnd         =   np.sort (tbnd)
        tbnd         =   np.concatenate ((np.array ([tbnd[0]-1]),tbnd,np.array ([tbnd[-1]+1])))
        
        if debug: 
            x_bnd        =   np.outer (v,tbnd) + x_old[:,None]
            plt.plot (x_bnd[0,:],x_bnd[1,:],'r.-')
            plt.axis ('equal')

        tmid         =   (tbnd[0:-1]+tbnd[1:]) / 2
        xmid         =   np.outer (v,tmid) + x_old[:,None]
        
        if debug: 
            plt.plot (xmid[0,:],xmid[1,:],'b.')
            plt.axis ('equal')

        tf           =   np.all (np.dot (Fw,xmid) + gw[:,None] >= 0, axis=0 )
        
        L            =   sum(tf)
        
        if L==0:
            print(" HIT-AND-RUN: ERROR - none found")
            xnew         =   x_old
        else:
            tbnd[-1]     =   +np.inf
            tbnd[0]      =   -np.inf
            inc          =   np.asarray(np.where(tf))
            tLeft        =   tbnd[inc]
            tRight       =   tbnd[inc+1]
    
            if len(inc)>1:
                intcdfLeft   =   norm.cdf(tLeft,loc=t_mean_sc,scale=1)
                intcdfRight  =   norm.cdf(tRight,loc=t_mean_sc,scale=1)
                mass2        =   intcdfRight-intcdfLeft
                intfrac2     =   mass2/sum(mass2) 
            else:
                intfrac2     =   np.array([1])
        
            index        =   np.where (cdf <= np.cumsum (intfrac2))
            cdfmax       =   np.sum (intfrac2[index])
            index        =   np.asarray (np.uint (index[0]))
            cdfloc       =   (cdf - cdfmax + intfrac2[index]) / intfrac2[index]
        
            # Note that boundaries a and b are specified relative to the standard normal distributions
            a            =   tLeft[index,0]
            b            =   tRight[index,0]
            m            =   t_mean[0]
            tnew         =   truncnorm.ppf (cdfloc, a-m, b-m, loc=m, scale=1)
            xnew         =   np.outer (v,tnew) + x_old[:,None]
            
            if debug: 
                plt.plot (xnew[0],xnew[1],'mo')
            
            if np.all (np.dot (Fw,xnew) + gw[:,None] >= 0):
                if debug:
                    print ("OK")
                pass
            else:
                print (" HIT-AND-RUN:  ERROR")
            
            xnew         =   xnew[:,0] 
            
        if visual & (D==2):
            ynew     =   self.unwhiten(xnew)
            plt.plot(ynew[0],ynew[1],'ko')
            
        return xnew
    
    def sample_hmc(self,x0,verbose=False,visual=False,Tau=None,AbsTol=None):
        
        """HAMILTONIAN MOTRE CARLO SAMPLER
        
        The purpose of :py:func:`.truncmvn.sample_hmc` is to obtain a single sample from the TMVN by means of a single iteration of the Hamiltonian Monte Carlo sampler, starting from a provided sample *x0*.
        
        Parameters
        ----------
        x0 : Initial sample (*D*-dimensional numpy array)
        verbose       : Boolean indicator for command line output (True/False)
        visual       : Boolean indicator for graphical output (True/False)
        Tau : Simulated time for particle trajectory, defaults to pi/2
        AbsTol : Required accuracy for bounces, defaults to 1e4 times the machine precision
    
        Returns
        -------
        x1 : New sample (*D*-dimensional numpy array)
        
        """
        
        if Tau is None:
            Tau=np.pi/2
            
        if AbsTol is None:
            AbsTol=10000*np.finfo(float).eps
        
        x0           =   np.asarray (x0, dtype=np.float)
        
        D            =   self.dim
        Fw           =   self.Fw
        F2norm       =   self.Fw2
        gw           =   self.gw
        
        Ft           =   Fw.T
        
        debug        =   False 
        
        trying       =   True
        trial_count  =   0
        
        while trying:
            
            stop        =   False
            j           =   0
            trial_count =   trial_count +1
            bouncenum   =   0
            
            #print("Trials: "+str(trial_count))
        
            v0          =   norm.rvs (size=(self.dim))
            #v0 = np.array([3,2]) # This line is used to debug
        
            x           =   x0
            tt          =   0       # records how much time the particle already moved
            
            while True:
                
                #print("Bounces: "+str(bounce_count))
            
                a           =   v0
                a           =   np.real (a)
                b           =   x
                
                fa          =   Fw.dot(a)
                fb          =   Fw.dot(b)
            
                U           =   np.sqrt(fa**2 + fb**2)
                phi         =   np.arctan2(-fa,fb)       # -pi < phi < +pi
                
                pn          =   np.abs(gw/U)<=1 # these are the walls that may be hit
            
                if pn.any():
                    inds        =   np.where (pn)[0]
                    phn         =   phi[inds]
                
                    t1          =  -phn + np.arccos (-gw[pn]/U[pn])  # time at which coordinates hit the walls
                
                    #if there was a previous reflection (j>0)
                    # and there is a potential reflection at the sample plane
                    # make sure that a new reflection at j is not found because of numerical error
                    if j>0:
                        if pn[j]==1:
                            cs     =   np.cumsum (pn)
                            indj   =   cs[j]-1
                            tt1    =   t1[indj]
                            if (np.abs (tt1) < AbsTol) | (np.abs (tt1-2*np.pi) < AbsTol):
                                t1[indj]  =   np.inf
            
                    m_ind      =   np.argmin(t1)
                    mt         =   t1[m_ind]
                    j          =   inds[m_ind]
                else:
                    mt         =   Tau
        
                
                tt          =   tt + mt  # elapsed time along current trajectory
            
                if tt >= Tau :
                    # target time has lapsed
                    mt          =   mt-(tt-Tau)
                    stop        =   True
                    
                # simulate new particle position and momentum (before bounce)
                x           =   a * np.sin (mt)   +   b * np.cos (mt)
                v           =   a * np.cos (mt)   -   b * np.sin (mt)
                
                if visual & (D==2):
                    tsim_       =   np.linspace(0,mt,100) 
                    xsim_       =   np.outer (a, np.sin (tsim_))   +   np.outer (b, np.cos (tsim_))
                    ysim_       =   self.unwhiten(xsim_)
                    plt.plot(ysim_[0,:],ysim_[1,:],'-',Color=np.ones(3)*.73)
                    y           =   self.unwhiten(x)
                    plt.plot(y[0],y[1],'.',Color=np.ones(3)*.73)
            
                if stop:
                    break
                else:
                    # compute momentum (after bounce)
                    qj          =   Fw[j,:].dot (v) / F2norm[j]
                    v0          =   v - 2 * qj * Ft[:,j]
                
                    bouncenum   =   bouncenum + 1
                    
            # at this point we have a sampled value X, but due to possible
            # numerical instabilities we check that the candidate X satisfies the
            # constraints before accepting it.
            if np.all (Fw.dot (x) + gw > 0):
                trying      =   False
            else:
                print ("  HMC - reject due to numerical error")
        
        xnew         =   x[:] 
        
        if visual & (D==2):
            ynew     =   self.unwhiten(xnew)
            plt.plot(ynew[0],ynew[1],'ko')
            
            
        return xnew, bouncenum
    
    def unwhiten(self,x):
        """REVERSE AFFINE TRANSFORMATION
        (from standardized basis to original basis)
        
        The purpose of :py:func:`.truncmvn.unwhiten` is to transform a point representation in the standardized space to its equivalent representation in the original space.
        
        Parameters
        ----------
        x : *D*-dimensional vector in standardized space
    
        Returns
        -------
        y    : *D*-dimensional vector in original space
    
        """
        
        if self.verbose:
            print ("Unwhiten")
        
        mu        =   self.mu[:,None]
        onedim    =   len (np.shape (x) )==1
        if onedim:
            x         =   x[:,None]
        
        # ==========================
        #   UNDO ROTATION
        # ==========================
        if self.cov:
            R         =   self.cholM
            yc        =   R.T.dot (x)
        else:
            R_linvC   =   self.linvR
            yc        =   R_linv.dot (x)
        
        # ==========================
        #   UNDO CENTERING
        # ==========================
        
        y         =   yc  +  mu
        
        # ==========================
        #   REPORT
        # ==========================
        
        if onedim:
            y         =   y[:,0]
            
        return y
        
    def whiten(self,y):
        """APPLY AFFINE TRANSFORMATION
        (from standardized basis to original basis)
        
        The purpose of :py:func:`.truncmvn.whiten` is to transform a point representation in the original space to its equivalent representation in the standardized space .
        
        Parameters
        ----------
        y    : *D*-dimensional vector in original space
    
        Returns
        -------
        x : *D*-dimensional vector in standardized space
    
        """
        
        if self.verbose:
            print ("Whiten")
        
        mu        =   self.mu
        onedim    =   len (np.shape (y) )==1
        if onedim:
            y         =   y[:,None]
            mu        =   mu[:,None]
        
        # ==========================
        #   CENTERING
        # ==========================
        yc        =   y  -  mu
        
        # ==========================
        #   ROTATION
        # ==========================
        if self.cov:
            R_linv    =   self.linvR
            x         =   R_linv.T.dot (yc)
        else:
            R         =   self.cholM
            x         =   R.dot (yc)
            
        # ==========================
        #   REPORT
        # ==========================
        if onedim:
            x          =   x[:,0]
            
        return x
    
    @property
    def cholM(self): 
        """Property: Cholesky factor of the covariance/precision matrix """
        
        if self.verbose:
            print ("Computing cholM")
        
        R         =   linalg.cholesky (self.M)
        
        return R
    
    @property
    def dim(self): 
        """Property: Number of dimensions/variables described by the distribution """
        
        if self.verbose:
            print ("Computing dim")
        
        dim       =   len (self.mu)
        
        return dim
    
    @property
    def gw(self):
        """Property: Right-hand side of linear inequality constraints expressed in standardized basis """
        
        if self.verbose:
            print ("Computing gw")
            
        if self.cov:
            mu        =   self.mu
        else:
            R_linv    =   self.linvR
            mu        =   R_linv.dot ( R_linv.T.dot (self.mu) )
            
        gw        =   self.g  +  self.F.dot (mu)
        
        return gw
    
    @property
    def Fw(self):
        """Property: Constraint matrix for linear inequality constraints expressed in standardized basis """
        
        if self.verbose:
            print ("Computing Fw")
        
        if self.cov:
            R         =   self.cholM
            Fw        =   self.F.dot (R.T)
        else:
            R_linv    =   self.linvR
            Fw        =   F.dot (R_linv)  
        return Fw
    
    @property
    def Fw2(self):
        """Property: Squared norms of the rows of the constraint matrix expressed in standardized basis (self.Fw)"""
        
        Fw        =   self.Fw
        F2norm    =   np.sum (Fw*Fw,1) # squared norm of the rows of F, needed for reflecting the velocity
        
        return F2norm
    
    @property
    def linvR(self):
        """Property: Left-inverse of the Cholesky factor of the covariance/precision matrix  """
        
        if self.verbose:
            print ("Computing linvR")
        
        R         =   self.cholM
        R_linv    =   linalg.solve (R.T.dot(R), R.T)
        
        return R_linv
    