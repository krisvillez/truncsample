TruncSample - Documentation
===========================

*Sampling from truncated multivariate distributions*

.. automodule:: truncsample.tmvn
    :members:

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/installation


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
