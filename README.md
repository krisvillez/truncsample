truncsample v1.0 - Sampling from truncated densities
by Kris Villez

Website:	http://tinyurl.com/eawag-spike
Contact: 	kris.villez@eawag.ch

This is a package for sampling from truncated multivariate densities. It 
currently includes bespoke algorithms designed for sampling from truncated 
multivariate normal densities

See the docs folder for documentation.
See the notebooks folder for examplary use.

--------------------------------------------------------------------------------
Last modified on 14 May 2019

